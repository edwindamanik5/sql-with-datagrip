# total komplain dalam masing masing bulan
select monthname(date_recieved) as Month, count(month(date_recieved)) as Total_Complain_Per_Month from customer_complaints group by month(date_recieved)

# Komplain yang memiliki Tags 'Older American'
select * from customer_complaints where tags = 'Older American'

# Buat sebuah view yang menampilkan data nama perusahaan, jumlah company response to consumer seperti tabel di bawah
select company,
    count(company_response_to_consumer = 'Closed') as closed,
    count(company_response_to_consumer = 'Closed with explanation') as Closed_with_explanation,
    count(company_response_to_consumer = 'Closed with non-monetary relief') as Closed_with_non_monetary_relief
from
    customer_complaints
group by
    company