# Menambah kolom jabatan pada karyawan
alter table karyawan
add jabatan varchar(100)
after departemen;

# Menampilkan isi tabel
select * from karyawan;
select * from departemen;

# Update jabatan
update karyawan set jabatan = 'Direktur' where nama = 'Rizki Saputra';
update karyawan set jabatan = 'Manajer' where nama = 'Farhan Reza';
update karyawan set jabatan = 'Manajer' where nama = 'Riyando Adi';
update karyawan set jabatan = 'Staff' where departemen in (2, 3, 4) and jabatan is null;

# menampilkan karyawan yang ada di bawah Farhan dan Riyando
select * from karyawan where jabatan = 'Staff';