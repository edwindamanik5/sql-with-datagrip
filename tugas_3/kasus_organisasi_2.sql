# membuat tabel company
create table company (
    id int auto_increment,
    nama varchar(100),
    alamat varchar(100),
    primary key (id)
);

# membuat tabel employee
create table employee (
    id int auto_increment,
    nama varchar(100),
    atasan_id int,
    company_id int,
    primary key (id),
    foreign key (company_id) references company(id)
);

# memasukkan data ke tabel company
insert into company
(nama, alamat)
values
('PT. JAVAN', 'Sleman'),
('PT. Dicoding', 'Bandung');

# memasukkan data ke tabel employee
insert into employee
(nama, atasan_id, company_id)
values
('Pak Budi', null, '1'),
('Pak Tono', '1', '1'),
('Pak Totok', '1', '1'),
('Bu Sinta', '2', '1'),
('Bu Novi', '3', '1'),
('Andre', '4', '1'),
('Dono', '4', '1'),
('Ismir', '5', '1'),
('Anto', '5', '1');

# Menampilkan isi tabel
select * from company;
select * from employee;

# Query untuk mencari siapa CEO
select * from employee limit 1;

# Query untuk mencari siapa staff
select * from employee where atasan_id in (4, 5);

# Query untuk mencari siapa direktur
select * from employee where atasan_id = 1;

# Query untuk mencari siapa manager
select * from employee where atasan_id in (2, 3);

# Query untuk mencari jumlah bawahan pak Budi
select count(nama) as JumlahBawahanPakBudi from employee where atasan_id is not null;

# Query untuk mencari jumlah bawahan Pak Tono
select count(nama) as JumlahBawahanPakTono from employee where atasan_id = 2;

# Query untuk mencari jumlah bawahan Pak Totok
select count(nama) as JumlahBawahanPakTotok from employee where atasan_id = 3;

# Query untuk mencari jumlah bawahan Bu Sinta
select count(nama) as JumlahBawahanBuSinta from employee where atasan_id = 4;

# Query untuk mencari jumlah bawahan Bu Novi
select count(nama) as JumlahBawahanBuNovi from employee where atasan_id = 5;

